+++
widget = "featurette"
headless = true
active = true
weight = 20

title = "Intereses"
subtitle = "En el LIDSOL encontrarás"

[[feature]]
  icon = "linux"
  icon_pack = "fab"
  name = "Software libre"

[[feature]]
  icon = "user-lock"
  icon_pack = "fas"
  name = "Seguridad Informática"

[[feature]]
  icon = "open-access"
  icon_pack = "ai"
  name = "Open access"

[[feature]]
  icon = "creative-commons"
  icon_pack = "fab"
  name = "Licencias abiertas"

#[[feature]]
#icon = "acm"
#  icon_pack = "ai"
#  name = "UNAM-FI"

[[feature]]
  icon = "language"
  icon_pack = "fas"
  name = "Procesamiento de lenguaje natural"

[[feature]]
  icon = "microchip"
  icon_pack = "fas"
  name = "Hardware Abierto"

[[feature]]
  icon = "vr-cardboard"
  icon_pack = "fas"
  name = "Realidad Virtual"

[[feature]]
  icon = "braille"
  icon_pack = "fas"
  name = "Aprendizaje Máquina"

[[feature]]
  icon = "docker"
  icon_pack = "fab"
  name = "DevOps"
+++
